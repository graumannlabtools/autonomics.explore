% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/02_plot_projected_samples.R
\name{plot_projected_samples}
\alias{plot_projected_samples}
\alias{plot_pca_samples}
\alias{plot_sma_samples}
\alias{plot_lda_samples}
\alias{plot_pls_samples}
\title{Plot PCA/LDA/PLS sample scores}
\usage{
plot_projected_samples(object, method = "pca", implementation = NULL,
  dims = 1:2, color_var = "subgroup",
  color_values = autonomics.plot::default_color_values(object, color_var),
  shape_var = autonomics.plot::default_shape_var(object), size_var = NULL,
  txt_var = autonomics.plot::default_txt_var(object), group_var = NULL,
  split_var = NULL, facet_var = split_var, scales = if
  (is.null(split_var)) "fixed" else "free", nrow = NULL, title = NULL,
  na.impute = FALSE, legend.position = "right")

plot_pca_samples(object, ...)

plot_sma_samples(object, ...)

plot_lda_samples(object, ...)

plot_pls_samples(object, ...)
}
\arguments{
\item{object}{SummarizedExperiment, eSet, or Elist}

\item{method}{'pca', 'lda', 'pls'}

\item{implementation}{which implementation of the method to use}

\item{dims}{dimensions}

\item{color_var}{svar mapped to color}

\item{color_values}{color values vector}

\item{shape_var}{svar mapped to shape}

\item{size_var}{svar mapped to size}

\item{txt_var}{svar mapped to txt}

\item{group_var}{svar mapped to group}

\item{split_var}{svar on which to split data prior to transformation}

\item{facet_var}{svar on which to facet sample biplot}

\item{scales}{'free' or 'fixed'}

\item{nrow}{integer}

\item{title}{title}

\item{na.impute}{TRUE or FALSE}

\item{legend.position}{character}

\item{...}{passed on to ggplot2::theme(legend.position = .)}
}
\description{
Plot PCA/LDA/PLS sample scores
}
\examples{
require(magrittr)
if (require(subramanian.2016)){
   object <- subramanian.2016::metabolon
   object \%>\% autonomics.explore::plot_pca_samples()
   object \%>\% autonomics.explore::plot_sma_samples()
   object \%>\% autonomics.explore::plot_lda_samples()
   object \%>\% autonomics.explore::plot_pls_samples()
   object \%>\% autonomics.explore::plot_pca_samples(color_var = 'condition')
   object \%>\% autonomics.explore::plot_pca_samples(color_var = 'condition', size = 'time')
   object \%>\% autonomics.explore::plot_lda_samples(color_var = 'condition', size = 'time')
   object \%>\% autonomics.explore::plot_pca_samples(facet_var = 'condition')
   object \%>\% autonomics.explore::plot_pca_samples(split_var = 'condition')
   object \%>\% autonomics.explore::plot_pca_samples(
                color_var = 'subgroup',  size_var = 'time', split_var = 'condition')
   object \%>\% autonomics.explore::plot_pca_samples(
                 color_var = 'condition', size_var = 'time', split_var = 'condition')
}
if (require(autonomics.data)){
   object <- autonomics.data::billing2016
   object \%>\% autonomics.explore::plot_pca_samples()
   object \%>\% autonomics.explore::plot_lda_samples()
}
if (require(billing.differentiation.data)){
   object <- billing.differentiation.data::protein.ratios
   object \%>\% autonomics.explore::plot_pca_samples()
   object \%>\% autonomics.explore::plot_lda_samples()
   object \%>\% autonomics.explore::plot_pls_samples()
}
if (require(atkin.2014)){
   object <- atkin.2014::soma
   object \%>\% autonomics.explore::plot_lda_samples()
   object \%>\% autonomics.explore::plot_lda_samples(group_var = 'block')
   object \%>\% autonomics.explore::plot_lda_samples(group_var = 'block', facet_var = 'condition')
   object \%>\% autonomics.explore::plot_lda_samples(group_var = 'block', split_var = 'condition')
}
}
